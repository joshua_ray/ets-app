package com.ets.app.service;

        import com.ets.app.domain.Person;
        import com.ets.app.domain.PersonRequest;
        import com.ets.app.entity.PersonEntity;
        import com.ets.app.exception.ResourceNotFoundException;
        import com.ets.app.repository.PersonRepository;
        import com.ets.app.service.mapper.PersonMapper;
        import org.junit.jupiter.api.Test;
        import org.junit.jupiter.api.extension.ExtendWith;
        import org.mockito.InjectMocks;
        import org.mockito.Mock;
        import org.springframework.test.context.junit.jupiter.SpringExtension;

        import java.util.Arrays;
        import java.util.List;

        import static org.hamcrest.CoreMatchers.is;
        import static org.hamcrest.MatcherAssert.assertThat;
        import static org.hamcrest.core.IsEqual.equalTo;
        import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class PersonServiceTest {

    @InjectMocks
    private PersonService subject;

    @Mock
    private PersonRepository mockPersonRepository;

    @Mock
    private PersonMapper mockPersonMapper;

    @Test
    public void getPersons() {
        PersonEntity personEntity = new PersonEntity();
        List<PersonEntity> personEntities = Arrays.asList(personEntity, personEntity);
        List<Person> expected = Arrays.asList(new Person(), new Person());
        when(mockPersonRepository.findAll()).thenReturn(personEntities);
        when(mockPersonMapper.mapPersonEntityListToPersonList(personEntities)).thenReturn(expected);

        List<Person> result = subject.getPersons();

        assertThat(result, is(expected));
        verify(mockPersonRepository).findAll();
        verify(mockPersonMapper).mapPersonEntityListToPersonList(personEntities);
    }

    @Test
    public void getPersonById() {
        PersonEntity personEntity = new PersonEntity();
        Person expected = new Person();
        when(mockPersonMapper.mapPersonEntityToPerson(personEntity)).thenReturn(expected);
        when(mockPersonRepository.findById(1)).thenReturn(java.util.Optional.of(personEntity));

        Person result = subject.getPersonById(1);

        assertThat(result, is(expected));
        verify(mockPersonRepository).findById(1);
        verify(mockPersonMapper).mapPersonEntityToPerson(personEntity);
    }

    @Test
    public void getPersonById_NonExistingId__ThrowsError() {
        try {
            when(mockPersonRepository.findById(1)).thenReturn(java.util.Optional.of(new PersonEntity()));
            subject.getPersonById(2);
        } catch (Throwable t) {
            assertThat(t instanceof ResourceNotFoundException, equalTo(true));
        }
    }

    @Test
    public void createPerson() {
        PersonEntity personEntity = new PersonEntity();
        Person expected = new Person();
        PersonRequest personRequest = new PersonRequest();
        when(mockPersonMapper.mapPersonRequestToPersonEntity(personRequest)).thenReturn(personEntity);
        when(mockPersonRepository.save(personEntity)).thenReturn(personEntity);
        when(mockPersonMapper.mapPersonEntityToPerson(personEntity)).thenReturn(expected);

        Person result = subject.createPerson(personRequest);

        assertThat(result, is(expected));
        verify(mockPersonRepository).save(personEntity);
        verify(mockPersonMapper).mapPersonRequestToPersonEntity(personRequest);
    }

    @Test
    public void updatePerson() {
        PersonEntity personEntity = new PersonEntity();
        Person expected = new Person();
        PersonRequest personRequest = new PersonRequest();

        when(mockPersonMapper.mapPersonEntityToPerson(personEntity)).thenReturn(expected);
        when(mockPersonRepository.findById(1)).thenReturn(java.util.Optional.of(personEntity));
        when(mockPersonMapper.mapPersonRequestToPersonEntity(personRequest)).thenReturn(personEntity);
        when(mockPersonRepository.save(personEntity)).thenReturn(personEntity);
        when(mockPersonMapper.mapPersonEntityToPerson(personEntity)).thenReturn(expected);

        Person result = subject.updatePerson(1, personRequest);

        assertThat(result, is(expected));
        verify(mockPersonRepository).findById(1);
        verify(mockPersonMapper, times(2)).mapPersonEntityToPerson(personEntity);
        verify(mockPersonRepository).save(personEntity);
        verify(mockPersonMapper).mapPersonRequestToPersonEntity(personRequest);
    }

    @Test
    public void deletePerson() {
        PersonEntity personEntity = new PersonEntity();

        when(mockPersonRepository.findById(1)).thenReturn(java.util.Optional.of(personEntity));
        doNothing().when(mockPersonRepository).delete(personEntity);

        subject.deletePerson(1);

        verify(mockPersonRepository, times(1)).delete(personEntity);
        verify(mockPersonRepository).findById(1);
    }


}