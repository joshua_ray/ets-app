package com.ets.app.service;

import com.ets.app.domain.Belonging;
import com.ets.app.domain.BelongingRequest;
import com.ets.app.domain.Person;
import com.ets.app.entity.BelongingEntity;
import com.ets.app.entity.PersonEntity;
import com.ets.app.repository.BelongingRepository;
import com.ets.app.service.mapper.BelongingMapper;
import com.ets.app.service.mapper.PersonMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class BelongingServiceTest {

    @InjectMocks
    private BelongingService subject;

    @Mock
    private BelongingRepository mockBelongingRepository;

    @Mock
    private BelongingMapper mockBelongingMapper;

    @Mock
    private PersonMapper mockPersonMapper;

    @Mock
    private PersonService mockPersonService;

    @Test
    public void getAllBelongingsByPersonId() {
        List<Belonging> expected = Arrays.asList(new Belonging(), new Belonging());
        List<BelongingEntity> belongingEntityList = Arrays.asList(new BelongingEntity(), new BelongingEntity());
        Person person = new Person();
        when(mockPersonService.getPersonById(1)).thenReturn(person);
        when(mockBelongingRepository.findByPersonEntityId(1)).thenReturn(belongingEntityList);
        when(mockBelongingMapper.mapBelongingEntityListToBelongingList(belongingEntityList)).thenReturn(expected);

        List<Belonging> result = subject.getAllBelongingsByPersonId(1);

        assertThat(result, is(expected));
        verify(mockPersonService).getPersonById(1);
        verify(mockBelongingRepository).findByPersonEntityId(1);
        verify(mockBelongingMapper).mapBelongingEntityListToBelongingList(belongingEntityList);
    }

    @Test
    public void createBelonging() {
        Person person = new Person();
        PersonEntity personEntity = new PersonEntity();
        BelongingRequest belongingRequest = new BelongingRequest();
        BelongingEntity belongingEntity = new BelongingEntity();
        belongingEntity.setPersonEntity(personEntity);
        Belonging expected = new Belonging();
        when(mockPersonService.getPersonById(1)).thenReturn(person);
        when(mockPersonMapper.mapPersonToPersonEntity(person)).thenReturn(personEntity);

        when(mockBelongingMapper.mapBelongingRequestToBelongingEntity(belongingRequest)).thenReturn(belongingEntity);
        when(mockBelongingRepository.save(belongingEntity)).thenReturn(belongingEntity);
        when(mockBelongingMapper.mapBelongingEntityToBelonging(belongingEntity)).thenReturn(expected);

        Belonging result = subject.createBelonging(1, belongingRequest);

        assertThat(result, is(expected));
        verify(mockPersonService).getPersonById(1);
        verify(mockPersonMapper).mapPersonToPersonEntity(person);
        verify(mockBelongingMapper).mapBelongingRequestToBelongingEntity(belongingRequest);
        verify(mockBelongingRepository).save(belongingEntity);
        verify(mockBelongingMapper).mapBelongingEntityToBelonging(belongingEntity);
    }

    @Test
    public void updateBelonging() {
        Person person = new Person();
        PersonEntity personEntity = new PersonEntity();
        BelongingRequest belongingRequest = new BelongingRequest();
        BelongingEntity belongingEntity = new BelongingEntity();
        belongingEntity.setPersonEntity(personEntity);
        Belonging expected = new Belonging();
        when(mockPersonService.getPersonById(1)).thenReturn(person);
        when(mockBelongingRepository.findById(1)).thenReturn(java.util.Optional.of(belongingEntity));
        when(mockBelongingRepository.save(belongingEntity)).thenReturn(belongingEntity);
        when(mockBelongingMapper.mapBelongingEntityToBelonging(belongingEntity)).thenReturn(expected);


        Belonging result = subject.updateBelonging(1, 1, belongingRequest);

        assertThat(result, is(expected));

        verify(mockPersonService).getPersonById(1);
        verify(mockBelongingRepository).findById(1);
        verify(mockBelongingRepository).save(belongingEntity);
        verify(mockBelongingMapper).mapBelongingEntityToBelonging(belongingEntity);
    }

    @Test
    public void deleteBelonging() {
        BelongingEntity belongingEntity = new BelongingEntity();

        when(mockBelongingRepository.findByIdAndPersonEntityId(1, 1)).thenReturn(java.util.Optional.of(belongingEntity));
        doNothing().when(mockBelongingRepository).delete(belongingEntity);

        subject.deleteBelonging(1, 1);

        verify(mockBelongingRepository, times(1)).delete(belongingEntity);
        verify(mockBelongingRepository).findByIdAndPersonEntityId(1, 1);

    }

}