package com.ets.app.controller;

import com.ets.app.domain.Person;
import com.ets.app.domain.PersonRequest;
import com.ets.app.service.PersonService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class PersonControllerTest {

    @Mock
    PersonService mockPersonService;

    @InjectMocks
    private PersonController subject;

    @Test
    public void getAllPersons(){
        List<Person> personList = new ArrayList<>();
        ResponseEntity<List<Person>> expected = new ResponseEntity<List<Person>>(personList, new HttpHeaders(), HttpStatus.OK);
        when(mockPersonService.getPersons()).thenReturn(personList);

        ResponseEntity<List<Person>> result = subject.getAllPersons();

        assertThat(result.getBody(), is(expected.getBody()));
        verify(mockPersonService).getPersons();
    }

    @Test
    public void getPerson(){
        Person person = Person.builder().id(1).firstName("testFirst").lastName("testLast").userName("testUserName").build();
        ResponseEntity<Person> expected = new ResponseEntity<Person>(person, new HttpHeaders(), HttpStatus.OK);
        when(mockPersonService.getPersonById(1)).thenReturn(person);

        ResponseEntity<Person> result = subject.getPerson(1);

        assertThat(result.getBody(), is(expected.getBody()));
        verify(mockPersonService).getPersonById(1);
    }

    @Test
    public void createPerson(){
        PersonRequest personRequest = new PersonRequest();
        Person person = Person.builder().id(1).firstName("testFirst").lastName("testLast").userName("testUserName").build();
        ResponseEntity<Person> expected = new ResponseEntity<Person>(person, new HttpHeaders(), HttpStatus.OK);
        when(mockPersonService.createPerson(personRequest)).thenReturn(person);

        ResponseEntity<Person> result = subject.createPerson(personRequest);

        assertThat(result.getBody(), is(expected.getBody()));
        verify(mockPersonService).createPerson(personRequest);
    }

    @Test
    public void updatePerson(){
        PersonRequest personRequest = new PersonRequest();
        Person person = Person.builder().id(1).firstName("testFirst").lastName("testLast").userName("testUserName").build();
        ResponseEntity<Person> expected = new ResponseEntity<Person>(person, new HttpHeaders(), HttpStatus.OK);
        when(mockPersonService.updatePerson(1, personRequest)).thenReturn(person);

        ResponseEntity<Person> result = subject.updatePerson(1,personRequest);

        assertThat(result.getBody(), is(expected.getBody()));
        verify(mockPersonService).updatePerson(1, personRequest);
    }

    @Test
    public void deletePerson(){
        ResponseEntity<?> expected = ResponseEntity.ok().build();
        doNothing().when(mockPersonService).deletePerson(1);

        ResponseEntity<?> result = subject.deletePerson(1);

        assertThat(result.getBody(), is(expected.getBody()));
        verify(mockPersonService).deletePerson(1);
    }

}