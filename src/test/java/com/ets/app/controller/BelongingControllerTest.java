package com.ets.app.controller;

import com.ets.app.domain.Belonging;
import com.ets.app.domain.BelongingRequest;
import com.ets.app.service.BelongingService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class BelongingControllerTest {

    @Mock
    BelongingService mockBelongingService;

    @InjectMocks
    private BelongingController subject;

    @Test
    public void getAllBelongingsByPersonId() {
        List<Belonging> belongingList = new ArrayList<>();
        ResponseEntity<List<Belonging>> expected = new ResponseEntity<List<Belonging>>(belongingList, new HttpHeaders(), HttpStatus.OK);
        when(mockBelongingService.getAllBelongingsByPersonId(1)).thenReturn(belongingList);

        ResponseEntity<List<Belonging>> result = subject.getAllBelongingsByPersonId(1);

        assertThat(result.getBody(), is(expected.getBody()));
        verify(mockBelongingService).getAllBelongingsByPersonId(1);
    }


    @Test
    public void createBelonging() {
        BelongingRequest belongingRequest = new BelongingRequest();
        Belonging belonging = Belonging.builder().id(1).type("testType").description("testDesc").build();
        ResponseEntity<Belonging> expected = new ResponseEntity<Belonging>(belonging, new HttpHeaders(), HttpStatus.OK);
        when(mockBelongingService.createBelonging(1, belongingRequest)).thenReturn(belonging);

        ResponseEntity<Belonging> result = subject.createBelonging(1, belongingRequest);

        assertThat(result.getBody(), is(expected.getBody()));
        verify(mockBelongingService).createBelonging(1, belongingRequest);
    }

    @Test
    public void updateBelonging() {

        BelongingRequest belongingRequest = new BelongingRequest();
        Belonging belonging = Belonging.builder().id(1).type("testType").description("testDesc").build();
        ResponseEntity<Belonging> expected = new ResponseEntity<Belonging>(belonging, new HttpHeaders(), HttpStatus.OK);
        when(mockBelongingService.updateBelonging(1, 1, belongingRequest)).thenReturn(belonging);

        ResponseEntity<Belonging> result = subject.updateBelonging(1, 1, belongingRequest);

        assertThat(result.getBody(), is(expected.getBody()));
        verify(mockBelongingService).updateBelonging(1, 1, belongingRequest);
    }

    @Test
    public void deleteBelonging() {
        ResponseEntity<?> expected = ResponseEntity.ok().build();
        doNothing().when(mockBelongingService).deleteBelonging(1, 1);

        ResponseEntity<?> result = subject.deleteBelonging(1, 1);

        assertThat(result.getBody(), is(expected.getBody()));
        verify(mockBelongingService).deleteBelonging(1, 1);
    }

}