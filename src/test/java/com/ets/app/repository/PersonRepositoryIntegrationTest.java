package com.ets.app.repository;

import com.ets.app.domain.PersonRequest;
import com.ets.app.entity.PersonEntity;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class PersonRepositoryIntegrationTest {

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void whenSaved_thenCorrectNumberOfPersons() {
        personRepository.save(PersonEntity.builder()
                .firstName("Joe")
                .lastName("Doe")
                .userName("jdoe")
                .build());
        List<PersonEntity> personEntityList = (List<PersonEntity>) personRepository.findAll();

        assertThat(personEntityList.size()).isEqualTo(1);
    }

}