package com.ets.app.repository;

import com.ets.app.entity.BelongingEntity;
import com.ets.app.entity.PersonEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class BelongingRepositoryTest {

    @Autowired
    private BelongingRepository belongingRepository;

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void whenSaved_thenCorrectNumberOfBelongings() {
        PersonEntity personEntity = personRepository.save(PersonEntity.builder()
                .firstName("Joe")
                .lastName("Doe")
                .userName("jdoe")
                .build());
        belongingRepository.save(BelongingEntity.builder()
                .type("testType")
                .description("testDesc")
                .personEntity(personEntity)
                .build());
        List<BelongingEntity> belongingEntities = (List<BelongingEntity>) belongingRepository.findByPersonEntityId(personEntity.getId());

        assertThat(belongingEntities.size()).isEqualTo(1);
    }
}
