package com.ets.app.service;


import com.ets.app.domain.Person;
import com.ets.app.domain.PersonRequest;
import com.ets.app.entity.PersonEntity;
import com.ets.app.exception.ResourceNotFoundException;
import com.ets.app.repository.PersonRepository;
import com.ets.app.service.mapper.PersonMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;


@Service
@Slf4j
public class PersonService {

    private PersonRepository personRepository;

    private PersonMapper personMapper;

    @Autowired
    public PersonService(PersonRepository personRepository, PersonMapper personMapper) {
        this.personRepository = personRepository;
        this.personMapper = personMapper;
    }

    public List<Person> getPersons() {
        List<PersonEntity> personEntities = personRepository.findAll();
        return personMapper.mapPersonEntityListToPersonList(personEntities);
    }

    public Person getPersonById(Integer id) {
        return personRepository.findById(id).map(personEntity1 -> personMapper.mapPersonEntityToPerson(personEntity1))
                .orElseThrow(() -> new ResourceNotFoundException("Person not found with id " + id));
    }
    @Transactional
    public Person createPerson(PersonRequest personRequest){
        return personMapper.mapPersonEntityToPerson(personRepository.save(personMapper.mapPersonRequestToPersonEntity(personRequest)));
    }

    @Transactional
    public Person updatePerson(Integer id, PersonRequest personRequest){
        this.getPersonById(id);
        PersonEntity personEntity = personMapper.mapPersonRequestToPersonEntity(personRequest);
        personEntity.setId(id);
        return personMapper.mapPersonEntityToPerson(personRepository.save(personEntity));
    }
    @Transactional
    public void deletePerson(Integer id) {
        PersonEntity personEntity = personRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Person not found with id " + id));
        personRepository.delete(personEntity);
    }
}
