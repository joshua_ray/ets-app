package com.ets.app.service;

import com.ets.app.model.TimestampModel;
import com.ets.app.service.util.DateTimeHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class TimestampService {

    private DateTimeHelper dateTimeHelper;

    @Autowired
    public TimestampService(DateTimeHelper dateTimeHelper){
        this.dateTimeHelper = dateTimeHelper;
    }
    public TimestampModel createTimestampMessage(){
        ZonedDateTime zdt = dateTimeHelper.getZonedDateTimeInPST();
        return TimestampModel.builder().timeStamp(zdt.format( DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm:ss z")))
                .message(dateTimeHelper.getGreetingMessage(zdt.getHour()))
                .build();
    }

}
