package com.ets.app.service;

import com.ets.app.domain.UserDataRequest;
import com.ets.app.domain.UserDataResponse;
import com.ets.app.service.util.DateTimeHelper;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

@Service
public class UserDataService {

    private DateTimeHelper dateTimeHelper;

    @Autowired
    public UserDataService(DateTimeHelper dateTimeHelper) {
        this.dateTimeHelper = dateTimeHelper;
    }

    public UserDataResponse createUserDataResponse(UserDataRequest userDataRequest) {
        ZonedDateTime zdt = dateTimeHelper.getZonedDateTimeInPST();
        StringBuilder message = new StringBuilder();
        message.append("Hello, ").append(userDataRequest.getName())
                .append(".  Have a great ")
                .append(dateTimeHelper.getDayOfTheWeekInFormattedCase(zdt.getDayOfWeek().toString()));
        return UserDataResponse.builder().message(message.toString()).build();
    }
}
