package com.ets.app.service.util;

import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Component
public class DateTimeHelper {
    public ZonedDateTime getZonedDateTimeInPST(){
        Instant now = Instant.now();
        ZoneId zoneId = ZoneId.of( "America/Los_Angeles" );
        ZonedDateTime zdt = now.atZone( zoneId );
        return zdt;
    }

    public String getDayOfTheWeekInFormattedCase(String dayOfTheWeekUpperCase){
        return dayOfTheWeekUpperCase.charAt(0) + dayOfTheWeekUpperCase.substring(1).toLowerCase();
    }
    public String getGreetingMessage(int hour){
        if(hour >=5 && hour <12 ){
            return "Good Morning";
        }else if(hour >=12 && hour <17 ){
            return "Good Afternoon";
        }else if(hour >=17 && hour <21){
            return "Good Evening";
        }else if((hour >=21 && hour <24) || (hour <5)){
            return "Good Night";
        }
        return null;
    }
}
