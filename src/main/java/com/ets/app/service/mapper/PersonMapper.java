package com.ets.app.service.mapper;

import com.ets.app.domain.Person;
import com.ets.app.domain.PersonRequest;
import com.ets.app.entity.PersonEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PersonMapper {

    public Person mapPersonEntityToPerson(PersonEntity personEntity) {
        return Person.builder().id(personEntity.getId())
                .firstName(personEntity.getFirstName())
                .lastName(personEntity.getLastName())
                .userName(personEntity.getUserName())
                .build();
    }

    public List<Person> mapPersonEntityListToPersonList(List<PersonEntity> personEntityList) {
        List<Person> personList = new ArrayList<>();
        personEntityList.forEach(personEntity -> personList.add(this.mapPersonEntityToPerson(personEntity)));
        return personList;
    }
    public PersonEntity mapPersonToPersonEntity(Person person) {
        return PersonEntity.builder()
                .firstName(person.getFirstName())
                .lastName(person.getLastName())
                .userName(person.getUserName())
                .build();
    }
    public PersonEntity mapPersonRequestToPersonEntity(PersonRequest personRequest) {
        return PersonEntity.builder()
                .firstName(personRequest.getFirstName())
                .lastName(personRequest.getLastName())
                .userName(personRequest.getUserName())
                .build();
    }
}
