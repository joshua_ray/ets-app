package com.ets.app.service.mapper;

import com.ets.app.domain.Belonging;
import com.ets.app.domain.BelongingRequest;
import com.ets.app.domain.Person;
import com.ets.app.entity.BelongingEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BelongingMapper {

    public Belonging mapBelongingEntityToBelonging(BelongingEntity belongingEntity){
        return Belonging.builder()
                .id(belongingEntity.getId())
                .type(belongingEntity.getType())
                .description(belongingEntity.getDescription())
                .build();
    }

    public List<Belonging> mapBelongingEntityListToBelongingList(List<BelongingEntity> belongingEntities){
        List<Belonging> belongingList = new ArrayList<>();
        belongingEntities.forEach(belongingEntity -> belongingList.add(this.mapBelongingEntityToBelonging(belongingEntity)));
        return belongingList;
    }
    public BelongingEntity mapBelongingRequestToBelongingEntity(BelongingRequest belongingRequest){
        return BelongingEntity.builder()
                .type(belongingRequest.getType())
                .description(belongingRequest.getDescription())
                .build();
    }
}
