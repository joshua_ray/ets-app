package com.ets.app.service;


import com.ets.app.domain.Belonging;
import com.ets.app.domain.BelongingRequest;
import com.ets.app.entity.BelongingEntity;
import com.ets.app.entity.PersonEntity;
import com.ets.app.exception.ResourceNotFoundException;
import com.ets.app.repository.BelongingRepository;
import com.ets.app.service.mapper.BelongingMapper;
import com.ets.app.service.mapper.PersonMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Slf4j
public class BelongingService {

    private PersonService personService;

    private PersonMapper personMapper;

    private BelongingRepository belongingRepository;

    private BelongingMapper belongingMapper;

    @Autowired
    public BelongingService(PersonService personService, BelongingRepository belongingRepository, BelongingMapper belongingMapper, PersonMapper personMapper) {
        this.belongingRepository = belongingRepository;
        this.belongingMapper = belongingMapper;
        this.personService = personService;
        this.personMapper = personMapper;
    }


    public List<Belonging> getAllBelongingsByPersonId(Integer personId) {
        personService.getPersonById(personId);
        return belongingMapper.mapBelongingEntityListToBelongingList(belongingRepository.findByPersonEntityId(personId));
    }

    public Belonging createBelonging(Integer personId, BelongingRequest belongingRequest) {
        PersonEntity personEntity = personMapper.mapPersonToPersonEntity(personService.getPersonById(personId));
        BelongingEntity belongingEntity = belongingMapper.mapBelongingRequestToBelongingEntity(belongingRequest);
        belongingEntity.setPersonEntity(personEntity);
        return belongingMapper.mapBelongingEntityToBelonging(belongingRepository.save(belongingEntity));
    }

    public Belonging updateBelonging(Integer personId, Integer belongingId, BelongingRequest belongingRequest) {
        personService.getPersonById(personId);
        return belongingRepository.findById(belongingId).map(belongingEntity -> {
            belongingEntity.setType(belongingRequest.getType());
            belongingEntity.setDescription(belongingRequest.getDescription());
            return belongingMapper.mapBelongingEntityToBelonging(belongingRepository.save(belongingEntity));
        }).orElseThrow(() -> new ResourceNotFoundException("BelongingId " + belongingId + "not found"));
    }

    public void deleteBelonging(Integer personId, Integer belongingId) {
        BelongingEntity belongingEntity = belongingRepository.findByIdAndPersonEntityId(belongingId, personId)
                .orElseThrow(() -> new ResourceNotFoundException("Belonging not found with id " + belongingId + " and personId " + personId));
        belongingRepository.delete(belongingEntity);
    }

}
