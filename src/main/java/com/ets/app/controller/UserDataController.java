package com.ets.app.controller;

import com.ets.app.domain.UserDataRequest;
import com.ets.app.domain.UserDataResponse;
import com.ets.app.service.UserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class UserDataController {

    private UserDataService userDataService;

    @Autowired
    public UserDataController(UserDataService userDataService){
        this.userDataService = userDataService;
    }

    @PostMapping("/userdata")
    public ResponseEntity<UserDataResponse> createBelonging(@Valid @RequestBody UserDataRequest userDataRequest) {
        return new ResponseEntity<UserDataResponse>(userDataService.createUserDataResponse(userDataRequest), new HttpHeaders(), HttpStatus.CREATED);
    }

}
