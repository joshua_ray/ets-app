package com.ets.app.controller;


import com.ets.app.domain.Person;
import com.ets.app.domain.PersonRequest;
import com.ets.app.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/persons")
    public ResponseEntity<List<Person>> getAllPersons() {
        return new ResponseEntity<List<Person>>(personService.getPersons(), new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/persons")
    public ResponseEntity<Person> createPerson(@Valid @RequestBody PersonRequest personRequest) {
        return new ResponseEntity<Person>(personService.createPerson(personRequest), new HttpHeaders(), HttpStatus.CREATED);
    }

    @GetMapping("/persons/{personId}")
    public ResponseEntity<Person> getPerson(@PathVariable Integer personId) {
        return new ResponseEntity<Person>(personService.getPersonById(personId), new HttpHeaders(), HttpStatus.OK);
    }

    @PutMapping("/persons/{personId}")
    public ResponseEntity<Person> updatePerson(@PathVariable Integer personId, @Valid @RequestBody PersonRequest personRequest) {
        return new ResponseEntity<Person>(personService.updatePerson(personId, personRequest), new HttpHeaders(), HttpStatus.CREATED);
    }

    @DeleteMapping("/persons/{personId}")
    public ResponseEntity<?> deletePerson(@PathVariable Integer personId) {
        personService.deletePerson(personId);
        return ResponseEntity.ok().build();
    }

}
