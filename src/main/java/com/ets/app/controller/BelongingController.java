package com.ets.app.controller;

import com.ets.app.domain.Belonging;
import com.ets.app.domain.BelongingRequest;
import com.ets.app.service.BelongingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class BelongingController {
    private final BelongingService belongingService;

    @Autowired
    public BelongingController(BelongingService belongingService) {
        this.belongingService = belongingService;
    }

    @GetMapping("/persons/{personId}/belongings")
    public ResponseEntity<List<Belonging>> getAllBelongingsByPersonId(@PathVariable(value = "personId") Integer personId) {
        return new ResponseEntity<List<Belonging>>(belongingService.getAllBelongingsByPersonId(personId), new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/persons/{personId}/belongings")
    public ResponseEntity<Belonging> createBelonging(@PathVariable(value = "personId") Integer personId,
                                   @Valid @RequestBody BelongingRequest belongingRequest) {
        return new ResponseEntity<Belonging>(belongingService.createBelonging(personId, belongingRequest), new HttpHeaders(), HttpStatus.CREATED);
    }

    @PutMapping("/persons/{personId}/belongings/{belongingId}")
    public ResponseEntity<Belonging>  updateBelonging(@PathVariable(value = "personId") Integer personId,
                                   @PathVariable(value = "belongingId") Integer belongingId,
                                   @Valid @RequestBody BelongingRequest belongingRequest) {
        return new ResponseEntity<Belonging>(belongingService.updateBelonging(personId, belongingId, belongingRequest), new HttpHeaders(), HttpStatus.CREATED);

    }

    @DeleteMapping("/persons/{personId}/belongings/{belongingId}")
    public ResponseEntity<?> deleteBelonging(@PathVariable(value = "personId") Integer personId,
                                           @PathVariable(value = "belongingId") Integer belongingId) {
        belongingService.deleteBelonging(personId, belongingId);
        return ResponseEntity.ok().build();
    }
}
