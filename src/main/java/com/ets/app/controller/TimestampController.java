package com.ets.app.controller;


import com.ets.app.model.TimestampModel;
import com.ets.app.service.TimestampService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class TimestampController {

    private TimestampService timestampService;

    @Autowired
    public TimestampController(TimestampService timestampService){
        this.timestampService = timestampService;
    }

    @GetMapping("/timestamp")
    public ResponseEntity<TimestampModel> getHello(){
        return new ResponseEntity<TimestampModel>(timestampService.createTimestampMessage(), new HttpHeaders(), HttpStatus.OK);
    }

}
