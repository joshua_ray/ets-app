package com.ets.app.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Setter
@Getter
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "persons")
public class PersonEntity extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Size(max = 100)
    @Column(unique = true, name = "user_name")
    private String userName;

    @NotNull
    @Size(max = 250)
    @Column(name = "first_name")
    private String firstName;

    @NotNull
    @Size(max = 250)
    @Column(name = "last_name")
    private String lastName;

}
