package com.ets.app.model;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Data
@Builder
public class TimestampModel {
    public String timeStamp;
    public String message;
}
