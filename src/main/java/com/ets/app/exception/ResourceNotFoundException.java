package com.ets.app.exception;

public class ResourceNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 8857419823404980799L;

    public ResourceNotFoundException() {
        super();
    }

    public ResourceNotFoundException(final String message) {
        super(message);
    }
}

