package com.ets.app.exception;

import javax.servlet.http.HttpServletRequest;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Setter
@Getter
@ControllerAdvice
public class ExceptionHandlerControllerAdvice {

    private String errorMessage;
    private String requestedURI;


    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public @ResponseBody ExceptionHandlerControllerAdvice handleResourceNotFound(final ResourceNotFoundException exception,
                                                                  final HttpServletRequest request) {

        this.setErrorMessage(exception.getMessage());
        this.callerURL(request.getRequestURI());

        return this;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody ExceptionHandlerControllerAdvice handleException(final Exception exception,
                                                           final HttpServletRequest request) {

        this.setErrorMessage(exception.getMessage());
        this.callerURL(request.getRequestURI());

        return this;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody ExceptionHandlerControllerAdvice methodArgumentNotValidExceptionHandler(final MethodArgumentNotValidException exception,
                                                                                 final HttpServletRequest request) {

        this.setErrorMessage(exception.getMessage());
        this.callerURL(request.getRequestURI());

        return this;
    }

    public void callerURL(final String requestedURI) {
        this.requestedURI = requestedURI;
    }

}
