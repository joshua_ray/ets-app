package com.ets.app.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.*;
import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonAutoDetect
public class BelongingRequest implements Serializable {

    private static final long serialVersionUID = -4444180817276985083L;

    private String type;
    private String description;
    private Long personId;

}
