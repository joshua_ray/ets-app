package com.ets.app.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.*;
import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonAutoDetect
public class Person implements Serializable{
    private static final long serialVersionUID = -241850314157721716L;
    private Integer id;
    private String userName;
    private String firstName;
    private String lastName;

}
