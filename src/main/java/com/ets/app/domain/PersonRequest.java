package com.ets.app.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.*;
import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonAutoDetect
public class PersonRequest implements Serializable{

    private static final long serialVersionUID = 8613808142518728026L;
    private String userName;
    private String firstName;
    private String lastName;

}
