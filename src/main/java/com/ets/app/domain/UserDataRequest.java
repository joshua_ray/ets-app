package com.ets.app.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.*;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonAutoDetect
public class UserDataRequest {

    @NonNull
    @NotBlank
    private String name;
}
