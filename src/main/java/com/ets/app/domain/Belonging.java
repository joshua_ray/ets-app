package com.ets.app.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.*;
import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonAutoDetect
public class Belonging implements Serializable {
    private static final long serialVersionUID = -523931199955449438L;

    private String type;
    private String description;
    private Integer personId;
    private Integer id;
}
